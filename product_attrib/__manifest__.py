# Copyright (C) 2018 - TODAY, Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Attribute Others",
    "summary": "Add Attribute",
    "version": "1.0.1.0.0",
    "license": "AGPL-3",
    "author": "Open Source Integrators, Flectra Community, Odoo Community Association (OCA)",
    "category": "Product Attribute",
    "website": "https://gitlab.com/art-in-heaven/odoo_custom_me",
    "depends": ["product"],
    "data": [
        "views/product_attrib.xml",

    ],
    "auto_install": False,
    "installable": True,
    "application": True,
}