# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _


class ProductAttrib(models.Model):
    _inherit = 'product.template'

    density = fields.Float(string='Density')
    width = fields.Integer(string='Width')
    thickness = fields.Integer(string='Thickness')
    length = fields.Integer(string='Length')

